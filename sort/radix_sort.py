"""radix sorting"""


def radix_sort(arr: list) -> list:
    """
    radix sorting

    :param arr: input array
    :param k: integer range [0..k-1]
    :returns: sorted array
    """
    k = 10
    for d in range(3):
        counter = [0] * k
        output = [0] * len(arr)
        for x in arr:
            counter[x] = counter[x] + 1
        for i in range(1, k):
            counter[i] = counter[i] + counter[i-1]
        for x in arr[::-1]:
            output[counter[x] - 1] = x
            counter[x] = counter[x] - 1
        arr = output
    return output

if __name__ == "__main__":
    ARR = [329, 457, 657, 839, 436, 720, 355]
    print(radix_sort(ARR))
