"""Counting sorting"""


def counting_sort(arr: list, k: int) -> list:
    """
    Counting sorting

    :param arr: input array
    :param k: integer range [0..k-1]
    :returns: sorted array
    """
    counter = [0] * k
    output = [0] * len(arr)
    for x in arr:
        counter[x] = counter[x] + 1
    for i in range(1, k):
        counter[i] = counter[i] + counter[i-1]
    for x in arr[::-1]:
        output[counter[x] - 1] = x
        counter[x] = counter[x] - 1
    return output

if __name__ == "__main__":
    ARR = [2, 5, 3, 0, 2, 3, 0, 3]
    print(counting_sort(ARR, 6))
