"""Insertion sorting"""


def insertion_sort(a: list) -> list:
    """
    Insertion sorting

    :param arr: input array
    :returns: sorted array
    """
    for i in range(1, len(a)):
        for j in range(i, 0, -1):
            if a[j - 1] > a[j]:
                a[j-1], a[j] = a[j], a[j-1]
    return a

if __name__ == "__main__":
    ARR = [41, 234, 123, -1, 1, 99]
    print(insertion_sort(ARR))
