"""quick sorting"""

def quick_sort2(arr: list, left_bound: int, right_bound: int):
    """
    :param arr: input array
    :param left_bound: left boundary of arr
    :param rightt_bound: right boundary of arr
    :returns: sorted array
    """
    if right_bound - left_bound < 1:
        return
    pivot = arr[right_bound]
    left, right = left_bound, right_bound - 1
    while left < right:
        if arr[left] > pivot:
            if arr[right] < pivot:
                arr[left], arr[right] = arr[right], arr[left]
            else:
                right -= 1
        else:
            left += 1
    if pivot < arr[left]:
        pivot_ix = left
        arr[left], arr[right_bound] = arr[right_bound], arr[left]
    else:
        pivot_ix = right_bound
    quick_sort2(arr, left_bound, pivot_ix - 1)
    quick_sort2(arr, pivot_ix + 1, right_bound)


def quick_sort(arr: list) -> list:
    """
    :param arr: input array
    :returns: sorted array
    """
    quick_sort2(arr, 0, len(arr) - 1)
    return arr


if __name__ == "__main__":
    ARR = [41, 234, 123, -1, 1, 1, 99]
    print(quick_sort(ARR))

    ARR = [4, 6, 54, 47, 61]
    print(quick_sort(ARR))
