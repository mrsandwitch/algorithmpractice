"""merge sorting"""

def merge_sort(a: list) -> list:
    """
    merge sorting

    :param arr: input array
    :returns: sorted array
    """
    if len(a) == 1:
        return a
    a1 = merge_sort(a[:len(a)//2])
    a2 = merge_sort(a[len(a)//2:])
    m = [0] * (len(a1) + len(a2))
    pr1, pr2, i = 0, 0, 0
    while i < len(m) and pr1 < len(a1) and pr2 < len(a2):
        if(a1[pr1] < a2[pr2]):
           m[i] = a1[pr1]
           pr1 += 1
        else:
           m[i] = a2[pr2]
           pr2 += 1
        i += 1

    for k in range(pr1, len(a1)):
        m[i] = a1[k]
        i += 1

    for k in range(pr2, len(a2)):
        m[i] = a2[k]
        i += 1

    return m

if __name__ == "__main__":
    ARR = [41, 234, 123, -1, 1, 99]
    print(merge_sort(ARR))
