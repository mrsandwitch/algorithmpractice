'''
For verifying sorting result
'''
import time
import numpy as np
from insertion_sort import insertion_sort
from merge_sort import merge_sort
from bubble_sort import bubble_sort
from quick_sort import quick_sort

TESTS = [
    ("bubble sort", bubble_sort),
    ("insertion sort", insertion_sort),
    ("merge_sort", merge_sort),
    ("quick_sort", quick_sort),
]

for t in TESTS:
    for i in range(10):
        start = time.time()
        arr = np.random.randint(1, 101, 50)
        ori = arr2 = arr
        ground = np.sort(arr)
        result = t[1](arr2)
        if not np.array_equal(ground, result):
            print("%s is incorrect." % t[0])
            print("ori:", ori)
            print("ground:", ground, "estimate:", result)
