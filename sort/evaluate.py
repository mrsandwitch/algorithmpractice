'''
Evaluate performance of different sorting algorithm
'''
import time
import numpy as np
from insertion_sort import insertion_sort
from merge_sort import merge_sort
from bubble_sort import bubble_sort
from quick_sort import quick_sort


TESTS = [
    ("bubble sort", bubble_sort),
    ("insertion sort", insertion_sort),
    ("merge sort", merge_sort),
    ("quick_sort", quick_sort),
]

for t in TESTS:
    start = time.time()
    print("--- %s ---" % t[0])
    for i in range(10):
        arr = np.random.randint(1, 101, 2000)
        result = t[1](arr)
    print("--- %s seconds ---" % (time.time() - start))
