import time
from insertion_sort import insertion_sort
from merge_sort import merge_sort
from bubble_sort import bubble_sort
import numpy as np


start = time.time()
for i in range(1000):
    a = [1] * 100000
    a.insert(50, 2)
print("--- %s seconds ---" % (time.time() - start))

start = time.time()
for i in range(1000):
    a = [1] * 100000
    a2 = a.append(1)
    for j in range(10000, 5000, -1):
        a[j] = a[j-1]
    a[5000] = 2
    #a.insert(50, 2)
print("--- %s seconds ---" % (time.time() - start))
