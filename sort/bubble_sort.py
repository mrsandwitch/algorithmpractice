"""Bubble sorting"""


def bubble_sort(a: list) -> list:
    """
    Bubble sorting

    :param arr: input array
    :returns: sorted array
    """
    for i in range(len(a)):
        for j in range(len(a)-i-1):
            if a[j + 1] < a[j]:
                a[j+1], a[j] = a[j], a[j+1]
    return a

if __name__ == "__main__":
    ARR = [41, 234, 123, -1, 1, 99]
    print(bubble_sort(ARR))
